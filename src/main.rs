use anyhow::{Result};
use qdrant_client::prelude::*;
use qdrant_client::qdrant::{
    CreateCollection, PointStruct, SearchPoints, Condition, Filter, VectorParams, VectorsConfig
};
use serde_json::json;
use std::convert::TryInto;

use qdrant_client::qdrant::vectors_config::Config;

#[tokio::main]
async fn main() -> Result<()> {
    let client = initialize_qdrant_client().await?;

    let collection_name = "country_collection";
    create_collection(&client, collection_name).await?;
    ingest_vectors(&client, collection_name).await?;
    query_vectors(&client, collection_name).await?;
    query_vectors_with_filter(&client, collection_name).await?;

    Ok(())
}

async fn initialize_qdrant_client() -> Result<QdrantClient> {
    let config = QdrantClientConfig::from_url("http://localhost:6334");
    let client = QdrantClient::new(Some(config))?;
    Ok(client)
}

async fn create_collection(client: &QdrantClient, collection_name: &str) -> Result<()> {
    let _ = client.delete_collection(collection_name).await;

    client.create_collection(&CreateCollection {
        collection_name: collection_name.into(),
        vectors_config: Some(VectorsConfig {
            config: Some(Config::Params(VectorParams {
                size: 4,
                distance: Distance::Cosine.into(),
                ..Default::default()
            })),
        }),
        ..Default::default()
    }).await?;

    Ok(())
}

async fn ingest_vectors(client: &QdrantClient, collection_name: &str) -> Result<()> {
    let points = vec![
        PointStruct::new(
            1,
            vec![0.11, 0.72, 0.86, 0.64],
            json!({"country": "Germany"}).try_into().unwrap(),
        ),
        PointStruct::new(
            2,
            vec![0.25, 0.91, 0.85, 0.21],
            json!({"country": "UK"}).try_into().unwrap(),
        ),
        PointStruct::new(
            3,
            vec![0.18, 0.82, 0.79, 0.43],
            json!({"country": "France"}).try_into().unwrap(),
        ),
        PointStruct::new(
            4,
            vec![0.48, 0.31, 0.64, 0.95],
            json!({"country": "USA"}).try_into().unwrap(),
        ),
        PointStruct::new(
            5,
            vec![0.97, 0.72, 0.44, 0.35],
            json!({"country": "Japan"}).try_into().unwrap(),
        ),
        PointStruct::new(
            6,
            vec![0.41, 0.91, 0.99, 0.53],
            json!({"country": "Australia"}).try_into().unwrap(),
        ),
        PointStruct::new(
            7,
            vec![0.86, 0.21, 0.37, 0.74],
            json!({"country": "Russia"}).try_into().unwrap(),
        ),
        PointStruct::new(
            8,
            vec![0.32, 0.49, 0.25, 0.89],
            json!({"country": "China"}).try_into().unwrap(),
        ),
        PointStruct::new(
            9,
            vec![0.71, 0.54, 0.62, 0.28],
            json!({"country": "Brazil"}).try_into().unwrap(),
        ),
        PointStruct::new(
            10,
            vec![0.60, 0.81, 0.92, 0.35],
            json!({"country": "South Africa"}).try_into().unwrap(),
        ),
    ];

    client.upsert_points_blocking(collection_name, None, points, None).await.unwrap();

    Ok(())
}

async fn query_vectors(client: &QdrantClient, collection_name: &str) -> Result<()> {
    let search_result = client
        .search_points(&SearchPoints {
            collection_name: collection_name.into(),
            vector: vec![0.2, 0.3, 0.4, 0.5],
            limit: 3,
            with_payload: Some(true.into()),
            ..Default::default()
        })
        .await
        .unwrap();

    println!("Search Results:");

    for (index, point) in search_result.result.iter().enumerate() {
        let formatted_payload = serde_json::to_string_pretty(&point.payload)
            .unwrap_or_else(|_| "Unable to format payload".to_string());

        println!("Point {}: ", index + 1);
        println!(" - Payload: {}", formatted_payload);
        println!(" - Score: {}", point.score); 
        println!(); 
    }

    Ok(())
}

async fn query_vectors_with_filter(client: &QdrantClient, collection_name: &str) -> Result<()> {
    let search_result = client
        .search_points(&SearchPoints {
            collection_name: collection_name.into(),
            vector: vec![0.2, 0.3, 0.4, 0.5],
            filter: Some(Filter::all([Condition::matches(
                "country",
                "USA".to_string(),
            )])),
            limit: 2,
            with_payload: Some(true.into()),
            ..Default::default()
        })
        .await
        .unwrap();

    println!("Search Results After Filter:");

    for (index, point) in search_result.result.iter().enumerate() {
        let formatted_payload = serde_json::to_string_pretty(&point.payload)
            .unwrap_or_else(|_| "Unable to format payload".to_string());
        println!("Point {}: ", index + 1);
        println!(" - Payload: {}", formatted_payload);
        println!(" - Score: {}", point.score); 
        println!(); 
    }

    Ok(())
}
