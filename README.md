Database connection:
![database_connection](<Database Connection.jpg>)

Ingest data into database:
![ingest_data](ingest_data.jpg)

Perform queries:
![perform_query_1](perform_query_1.jpg)

![perform_query_2](perform_query_2.jpg)

Query 1 result:
![query_1_result](query_1_result.jpg)

Query 2 result:
![query_2_resulty](query_2_result.jpg)

Database logs after the query operation:
![database_logs](database_logs.jpg)